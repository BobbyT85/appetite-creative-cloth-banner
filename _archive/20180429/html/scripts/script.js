var container, stats, camera, scene, renderer, clothGeometry, object;

var pinsFormation = [
	[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
	[]
], pins = pinsFormation[0];


// Change these variables to link to a new texture and amend the drop dropDelay
// The image should be the same as the one in the html
var imageTexture = "images/image.jpg",
		dropDelay	= 10;


if (!Detector.webgl) Detector.addGetWebGLMessage();





function init() {
	container = document.createElement('div');
	// container.className = "shadowed";
	document.body.appendChild(container);

	// scene
	scene = new THREE.Scene();


	// camera
	camera = new THREE.PerspectiveCamera( 18, window.innerWidth / window.innerHeight, 1, 8000 );
	camera.position.set( 0, -43, 900 );


	// lights
	var light, materials, d = 300;

	scene.add(new THREE.AmbientLight(0x666666));

	light = new THREE.DirectionalLight(0xdfebff, 1);
	light.position.set(50, 200, 100);
	light.position.multiplyScalar(1.3);

	light.castShadow = true;

	light.shadow.mapSize.width = 1024;
	light.shadow.mapSize.height = 1024;

	light.shadow.camera.left = -d;
	light.shadow.camera.right = d;
	light.shadow.camera.top = d;
	light.shadow.camera.bottom = -d;

	light.shadow.camera.far = 1000;

	scene.add(light);


	// cloth material
	var loader = new THREE.TextureLoader(), clothTexture = loader.load(imageTexture);
	clothTexture.anisotropy = 16;

	var clothMaterial = new THREE.MeshLambertMaterial({
		map					: clothTexture,
		// side				: THREE.BackSide,
		side				: THREE.DoubleSide,
		alphaTest		: 0.5
	});


	// cloth geometry
	clothGeometry = new THREE.ParametricGeometry( clothFunction, cloth.w, cloth.h );


	// cloth mesh
	object = new THREE.Mesh(clothGeometry, clothMaterial);
	object.position.set(0, 0, 0);
	object.castShadow = true;
	object.receiveShadow = true;
	scene.add(object);

	object.customDepthMaterial = new THREE.MeshDepthMaterial({
		depthPacking	: THREE.RGBADepthPacking,
		map						: clothTexture,
		alphaTest			: 0.5
	});


	// renderer
	renderer = new THREE.WebGLRenderer({antialias: true, alpha:true});
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor(0x000000, 0);
	// Used by Bobby Tesalona - Bobby T Limited
	container.appendChild(renderer.domElement);
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.shadowMap.enabled = true;


	window.addEventListener('resize', onWindowResize, false);


	animate();


	//Drop banner
	setTimeout(function() { pins = pinsFormation[1]; }, dropDelay * 1000);
}


function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}


function animate() {
	requestAnimationFrame(animate);

	var time = Date.now(), windStrength = Math.cos(time / 7000) * 20 + 40;

	windForce.set(Math.sin(time / 2000), Math.cos(time / 3000), Math.sin(time / 1000));
	windForce.normalize();
	windForce.multiplyScalar(windStrength);

	simulate(time);
	render();
}

function render() {
	var p = cloth.particles, i = 0;

	for (i = 0, il = p.length; i < il; i++) clothGeometry.vertices[i].copy(p[i].position);

	clothGeometry.verticesNeedUpdate = true;

	clothGeometry.computeFaceNormals();
	clothGeometry.computeVertexNormals();

	renderer.render(scene, camera);
}
